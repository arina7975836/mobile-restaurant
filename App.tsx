import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,Image } from 'react-native';
import RootNavigator from "./src/navigation/Index";
import {Provider, useSelector} from "react-redux";
import {store} from './src/redux/store.js';
import {colors} from "./public/colors";

export default function App() {

  return (
      <>
          <Provider store={store}>
              <RootNavigator/>
            <StatusBar style="auto" />
          </Provider>
      </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'pink',
    alignItems: 'center',
    justifyContent: 'center',
  },
    loader:{
      width:150,
        height:150,
        alignSelf:'center',
    },
    loadingContainer:{
      position:'absolute',
        width:'100%',
        height:'50%',
        flexDirection:'column',
        justifyContent:'space-between',
        alignItems:'center',
        marginTop:80,
    },
    background:{
      flex:1,
        width:'100%',
        height:'100%',
    },
    restaurantName:{
      fontWeight:'bold',
        fontSize:50,
        color:colors.white,
        backgroundColor:"#342424",
        padding:7,
        paddingHorizontal:12,
        borderRadius:25,
    }
});
