export  const colors={
    primary:'#441414',
    lightBrown:'#654141',
    white:'#F9F3F3',
    lightGrey:'#DDD5D4',
    middleGrey:'#393939',
    darkGrey:'#101010',
    red:'#CD6464',
    notAllowedGrey:'#A09B9B',
    transparentGrey:'rgba(213,213,213)',
    beige:'#A28375',
}
