import {fetchBaseQuery,createApi} from "@reduxjs/toolkit/query/react";

export const restaurantGoodsApi=createApi({
    reducerPath:'restaurantGoodsApi',
    baseQuery:fetchBaseQuery({
        baseUrl:'http://192.168.1.176:5001/'
    }),
    endpoints:(builder) =>({
         getGoods:builder.query({query:()=>'api/Dish/'}),
         getCategories:builder.query({query:() =>({
                 url:'api/Category',
                 responseHandler: (response) => response.json(),
             })}),
    }),

})

export const{
     useGetGoodsQuery,
    useGetCategoriesQuery,
}=restaurantGoodsApi