import {createSlice} from "@reduxjs/toolkit";

export interface  DishServe{
    id:number;
    name:string;
    cost:number;
    img:string;
    isFav:boolean;
    servingNumber:number;
    isInBasket:boolean;
    categoryId:number;
}
export interface Category{
    id:number;
    name:string;
}


const initialState={
    favouriteList: [],
    basketList:[],
    menuDishes: [],
    categories:[],
    activeCategory:null,
    sortedMenu:[],
}

const dishSlice=createSlice({
    name:'dish',
    initialState,
    reducers:{
        addToFavouriteList:(state,action)=>{
            let removeFromList
            if(state.favouriteList.length!==0)
              removeFromList=state.favouriteList.find((item)=> item.id===action.payload.dish.id)

            if(removeFromList!==undefined && state.favouriteList.length!==0){
                let index=state.favouriteList.findIndex(item=>item.id===action.payload.dish.id)
                state.favouriteList.splice(index,1)
            }else{
                state.favouriteList.push(action.payload.dish)
            }

        },
        addToBasketList:(state,action)=>{
            const dish=state.menuDishes.find(item=>item.id===action.payload.dishId)
            let isDishInList=-1

            if(state.basketList.length!==0)
              isDishInList=state.basketList.findIndex(item=>item.dishId===action.payload.dishId) ?? -1

            if(isDishInList===-1){
                state.basketList.push({
                    dishId:dish.id,
                    name:dish.name,
                    amount: action.payload.amount ?? 1,
                    cost:dish.cost,
                    img:dish.img,
                    fullCost:dish.cost*(action.payload.amount ?? 1),
                })
            }
            if(isDishInList>-1){
                const index=state.basketList.findIndex(item=>item.dishId===action.payload.dishId)
                const item= state.basketList[index]

                state.basketList.splice(index,1,{
                    dishId:dish.id,
                    name:dish.name,
                    amount:action.payload?.amount ?? ++item.amount,
                    cost:dish.cost,
                    img:dish.img,
                    fullCost:(action.payload.amount ?? ++item.amount)*dish.cost,
                })
            }
        },
        editDishAmount:(state, action)=>{
            const index=state.basketList.findIndex(item=>item.id===action.payload.dishId)
            const dish=state.basketList[index]
            state.basketList.splice(index,1,{
                dishId:dish.id,
                name:dish.name,
                amount:action.payload.amount,
                cost:dish.cost,
                img:dish.img,
                fullCost:action.payload.amount*dish.cost,
            })
        },
        deleteFromBasketList:(state,action)=>{
            const index=state.basketList.findIndex(item=>item.dishId===action.payload.dishId)

            state.basketList.splice(index,1)
        },
        deleteFromFavList:(state,action)=>{
            const index=state.favouriteList.findIndex(item=>item.id===action.payload.dishId)
            state.favouriteList.splice(index,1)
        },
        setMenuDishes:(state,action)=>{
          const dishes=action.payload.dishes
            let arr=[]

            for(let dish of dishes){
                let item:DishServe={
                    id:dish.id,
                    cost:dish.cost,
                    img:dish.img,
                    name:dish.name,
                    isFav:false,
                    servingNumber:0,
                    isInBasket:false,
                    categoryId:dish.categoryId
                }
                arr.push(item)
                //if category is't a Drinks
                arr.filter(item=>item.categoryId!==1)
            }
            state.menuDishes.push(...arr)
            state.sortedMenu.push(...arr)

        },
        setCategories:(state,action)=>{
            for(let category of action.payload.categories){
                if(category.id!==1)
                    state.categories.push(
                        {
                            id:category.id,
                            name:category.name,
                        } as Category
                    )
            }
        },
        setActiveCategory:(state, action)=>{
            state.activeCategory=action.payload.category;
            state.sortedMenu=state.menuDishes.filter(item=>item.categoryId===action.payload.category.id)

            /* to show full menu */
            if(action.payload.category.id==6){
                state.sortedMenu.push(...state.menuDishes)
            }
        }
    }
})

export const{
    addToFavouriteList,
    addToBasketList,
    deleteFromBasketList,
    deleteFromFavList,
    setMenuDishes,
    setCategories,
    setActiveCategory,
    editDishAmount,
}=dishSlice.actions

export default dishSlice.reducer