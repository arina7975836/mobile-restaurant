import {configureStore} from "@reduxjs/toolkit";
import {restaurantGoodsApi} from "./restaurantGoodsApi";
import dishReducer from "./dishSlice";
import settingsReducer from './settingsSlice'

export const store=configureStore({
    reducer:{
        [restaurantGoodsApi.reducerPath]: restaurantGoodsApi.reducer,
        dish:dishReducer,
        settings:settingsReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(restaurantGoodsApi.middleware),
})