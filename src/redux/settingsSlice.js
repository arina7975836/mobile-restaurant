import {createSlice} from "@reduxjs/toolkit";

const initialState={
    loader:true
}

const settingsSlice=createSlice({
    name:'settingsSlice',
    initialState,
    reducers:{
        changeLoaderState:(state, action)=>{
            state.loader=false
        }
    }
})

export const{
    changeLoaderState,
}=settingsSlice.actions;

export default settingsSlice.reducer