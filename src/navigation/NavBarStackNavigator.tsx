import React from 'react';
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import Basket from '../pages/Basket'
import Profile from "../pages/Profile";
import Menu from "../pages/Menu";
import HeaderTitle from "../components/HeaderTitle";
import FavouriteItems from "../pages/FavouriteItems";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {colors} from "../../public/colors";
import {AntDesign, Feather} from "@expo/vector-icons";
import {StyleSheet} from 'react-native';
import NavBarBottom from "../components/NavBar";
import MenuStackNavigator from "./MenuStackNavigator";
import BasketStackNavigator from "./BasketStackNavigator";

const NavBarStack=createBottomTabNavigator()
const NavBarStackNavigator = () => {
    return (
        <NavBarStack.Navigator
            initialRouteName='MenuStack'

            screenOptions={{
                    headerStyle: {
                        backgroundColor: 'fff',
                    },
                tabBarStyle:{display:'none'}
                }}
        >
            <NavBarStack.Screen
                name='MenuStack'
                component={MenuStackNavigator}
                options={{
                    headerShown: false,
                }}
            />
            <NavBarStack.Screen
                name='FavItems'
                component={FavouriteItems}
                options={{
                    title:'Избранное',
                    headerLeft:()=>(
                        <HeaderTitle />
                    )
                }}
            />
            <NavBarStack.Screen
                name='Basket'
                component={BasketStackNavigator}
                options={{
                    headerShown: false,
                }}
            />
            <NavBarStack.Screen
                name='Profile'
                component={Profile}
                options={{
                    title:'Профиль',
                    headerShown: false
                }}
            />
        </NavBarStack.Navigator>
    );
};

const styles=StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingVertical:20,
        paddingHorizontal:45,
        borderTopLeftRadius:25,
        borderTopRightRadius:25,
        backgroundColor:'pink',
        height:70,
    }
})

export default NavBarStackNavigator;