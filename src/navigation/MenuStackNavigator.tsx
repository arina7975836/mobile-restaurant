import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import DishDetails from "../pages/DishDetails";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {createStackNavigator} from "@react-navigation/stack";
import Menu from "../pages/Menu";
import HeaderTitle from "../components/HeaderTitle";
import {colors} from "../../public/colors";

const MenuStack = createStackNavigator();
const MenuStackNavigator = () => {
    return (
        <MenuStack.Navigator initialRouteName='Menu'>
            <MenuStack.Screen name='Details'
                        component={DishDetails}
                        options={{
                         title:'',
                            headerStyle:{
                             backgroundColor:colors.lightBrown,
                            },
                            headerLeft:()=>(
                                <HeaderTitle darkBackGround={true}/>
                            )
                        }}
            />
            <MenuStack.Screen
                name='Menu'
                component={Menu}
                options={{
                    title:'Меню',
                }}
            />
        </MenuStack.Navigator>
    );
};

export default MenuStackNavigator;