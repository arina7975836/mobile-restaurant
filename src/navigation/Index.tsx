import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MenuStackNavigator from "./MenuStackNavigator";
import NavBarStackNavigator from "./NavBarStackNavigator";
import {StatusBar} from "expo-status-bar";
import Loader from "../pages/Loader";
import {useSelector} from "react-redux";
const RootNavigator = () => {
    let isLoading=useSelector((state)=> state.settings.loader)

    return (
        <>
            {
                isLoading ?
                    <Loader/> :
                    <NavigationContainer >
                        <StatusBar animated={true} backgroundColor="#fff" />
                        <NavBarStackNavigator/>
                    </NavigationContainer>
            }
        </>

    );
};

export default RootNavigator;