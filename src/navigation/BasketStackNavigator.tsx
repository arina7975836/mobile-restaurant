import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import DishDetails from "../pages/DishDetails";
import HeaderTitle from "../components/HeaderTitle";
import Menu from "../pages/Menu";
import Basket from "../pages/Basket";
import {colors} from "../../public/colors";
import MakeOrder from "../pages/MakeOrder";

const BasketStack = createStackNavigator();
const BasketStackNavigator = () => {
    return (
        <BasketStack.Navigator initialRouteName='Basket'>
            <BasketStack.Screen
                name='Basket'
                component={Basket}
                options={{
                    title:'Корзина',
                    headerLeft:()=>(
                        <HeaderTitle darkBackGround={false}/>
                    )
            }}
            />
            <BasketStack.Screen
                name='MakeOrder'
                component={MakeOrder}
                options={{
                    title:'Детали заказа',
                }}
            />
        </BasketStack.Navigator>
    );
};

export default BasketStackNavigator;