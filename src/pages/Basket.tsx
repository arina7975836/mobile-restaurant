import React, {useEffect, useState} from 'react';
import {StyleSheet, View,SafeAreaView,ScrollView,Pressable} from 'react-native'
import {Text} from 'react-native'
import NavBarBottom from "../components/NavBar";
import {useSelector} from "react-redux";
import {StatusBar} from "expo-status-bar";
import BasketDishCard from "../components/BasketDishCard";
import index from "../navigation";
import {colors} from "../../public/colors";
import {useNavigation} from "@react-navigation/native";

const Basket = () => {
    const basketList=useSelector((state)=>state.dish.basketList)

    let [deliveryPrice,setDeliveryPrice]=useState(500)
    let [totalPrice,setTotalPrice]=useState(0)

    const navigation=useNavigation()

    useEffect(()=>{
        let sum=0

        for(let dish of basketList){
            sum+=dish.amount*dish.cost
        }

        setTotalPrice(sum)
    },[basketList])

    return (
        <View style={{flex:1}}>
                <SafeAreaView style={styles.basketContainer}>
                    <ScrollView >
                        <View style={styles.dishList}>
                            {
                                basketList&&
                                basketList.map((item,index)=>(
                                <BasketDishCard
                                    key={item.id}
                                    id={item.dishId}
                                    name={item.name}
                                    price={item.cost}
                                    img={item.img}
                                    fullPrice={item.fullCost}
                                    amount={item.amount}
                                />
                                ))
                            }
                        </View>
                    </ScrollView>
                </SafeAreaView>
            <View style={styles.bottom}>
                <View style={styles.totalSum}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={styles.textConst}>Доставка: </Text>
                        <Text style={styles.deliveryText}>{totalPrice>1500 ? 0 : deliveryPrice}₽</Text>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={styles.textConst}>Итог: </Text>
                        <Text style={styles.deliveryText}>{totalPrice}₽</Text>
                    </View>
                </View>
                <Pressable style={styles.buttonToExecute} onPress={()=>navigation.navigate('MakeOrder')}>
                    <Text style={styles.textButton}>Продолжить</Text>
                </Pressable>
            </View>
            <NavBarBottom/>
        </View>
    );
};

const styles=StyleSheet.create({
    basketContainer:{
        flex:1,
        paddingTop: StatusBar.currentHeight,
        marginTop:4,
    },
    dishList:{
        width:'100%',
        flexDirection:'column',
        flexWrap:'nowrap',
        alignItems:'center',
        justifyContent:'center',
    },
    totalSum:{
        flexDirection:'column',
        alignItems:'flex-start',
        justifyContent:'center',
        borderRadius:12,

    },
    textConst:{
        fontSize:20,
        color:colors.middleGrey,
        fontWeight:'normal',
    },
    deliveryText:{
        fontSize:20,
        color:colors.middleGrey,
        fontWeight:'normal',
    },
    buttonToExecute:{
        backgroundColor:colors.primary,
        borderRadius:15,
        paddingHorizontal:10,
        paddingVertical:8,
        marginVertical:7,
        justifyContent:'center',
        alignItems:'center',
    },
    textButton:{
        fontSize:20,
        fontWeight:'bold',
        color:colors.white,
    },
    bottom:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal:10,
        borderTopColor:colors.lightGrey,
    }
})

export default Basket;