import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import background from "../../assets/backgroundLoader.jpg";
import loading from "../../assets/loader.gif";
import {colors} from "../../public/colors";
import {useDispatch, useSelector} from "react-redux";
import {useGetCategoriesQuery, useGetGoodsQuery} from "../redux/restaurantGoodsApi";
import {changeLoaderState} from "../redux/settingsSlice";
import {setMenuDishes,setCategories} from "../redux/dishSlice";

const Loader = () => {
    let dishes =useSelector((state)=> state.dish.menuDishes)

    const dispatch=useDispatch()

    const {data:dataDishes,isFetching:isFetchingGoods,error:errorGoods}=useGetGoodsQuery()
    const {data:dataCategories,isFetching:isFetchingCategories,error:errorCategory}=useGetCategoriesQuery()

    const set=async ()=>{
        await dispatch(changeLoaderState())
        await dispatch(setMenuDishes({dishes: dataDishes}))
        await dispatch(setCategories({categories:dataCategories}))
    }

    useEffect(()=>{
        if(dataCategories && dataDishes){
            set()
        }
    },[dataDishes,dataCategories])

    return (
        <>
                <View style={styles.container}>
                    <Image style={styles.background} source={background}/>
                    <View style={styles.loadingContainer}>
                        <Text style={styles.restaurantName}>Цони-Мацони</Text>
                        <Image style={styles.loader} source={loading}/>
                    </View>
                </View>
        </>

    );
};

const styles = StyleSheet.create({
    container: {
        position:"absolute",
        width:'100%',
        height:'100%',
        zIndex:10,
    },
    loader: {
        width: 150,
        height: 150,
        alignSelf: 'center',
    },
    loadingContainer: {
        position: 'absolute',
        width: '100%',
        height: '50%',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 80,
    },
    background: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    restaurantName: {
        fontWeight: 'bold',
        fontSize: 50,
        color: colors.white,
        backgroundColor: "#342424",
        padding: 7,
        paddingHorizontal: 12,
        borderRadius: 25,
    }
})
export default Loader;