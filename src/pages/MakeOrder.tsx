import React from 'react';
import {View, Text, Pressable, StyleSheet, Input, TextInput} from "react-native";
import {colors} from "../../public/colors";

const MakeOrder = () => {
    return (
        <View style={styles.container}>
            <View style={styles.contacts}>
                <Text style={styles.name}>Имя: </Text>
                <TextInput style={styles.input}
                           numberOfLines={1}
                           maxLength={14}/>
                <Text style={styles.name}>Телефон:</Text>
                <TextInput style={styles.input}
                           numberOfLines={1}
                           maxLength={11||12}
                />
            </View>
            <View style={styles.address}>
                <Text style={styles.name}>Адрес:</Text>
                <TextInput
                    style={styles.input}
                    numberOfLines={1}
                    maxLength={200}
                />
                <Text style={styles.name}>Пожелания к заказу:</Text>
                <TextInput
                    multiline
                    numberOfLines={4}
                    maxLength={300}
                    style={styles.commentsInput}
                />
            </View>
            <Pressable style={{marginTop:80}}>
                <Text style={styles.button}>Оформить</Text>
            </Pressable>
        </View>
    );
};

const styles=StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        alignItems:'center',
    },
    contacts:{
        width:'94%',
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        padding:10,
        marginVertical:15,
        backgroundColor:colors.lightGrey,
        borderRadius:15,
    },
    name:{
        fontSize:20,
        color:colors.middleGrey,
        fontWeight:'normal',
        marginBottom:7,
    },
    input:{
        alignSelf:'flex-start',
        backgroundColor:colors.white,
        fontSize:20,
        color:colors.middleGrey,
        fontWeight:'normal',
        width:'100%',
        borderRadius:15,
        padding:9,
    },
    address:{
        width:'94%',
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        padding:10,
        marginVertical:15,
        backgroundColor:colors.lightGrey,
        borderRadius:15,
    },
    commentsInput:{
        alignSelf:'flex-start',
        backgroundColor:colors.white,
        fontSize:20,
        color:colors.middleGrey,
        fontWeight:'normal',
        width:'100%',
        borderRadius:15,
        paddingTop:2,
    },
    button:{
        fontSize:20,
        fontWeight:'normal',
        paddingVertical:10,
        paddingHorizontal:12,
        borderRadius:15,
        color:colors.white,
        backgroundColor:colors.primary,
    },
})
export default MakeOrder;