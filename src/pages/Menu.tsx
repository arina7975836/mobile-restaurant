import React, {useEffect} from 'react';
import {SafeAreaView, ScrollView, View} from "react-native";
import NavBarBottom from '../components/NavBar';
import {StyleSheet} from "react-native";
import {Text} from 'react-native'
import Dish from "../components/Dish";
import {StatusBar} from "expo-status-bar";
import {useGetCategoriesQuery, useGetGoodsQuery} from '../redux/restaurantGoodsApi.js';
import {useDispatch, useSelector} from "react-redux";
import {addToBasketList, addToFavouriteList,setActiveCategory} from "../redux/dishSlice";
import {changeLoaderState} from '../redux/settingsSlice.js';
import Loader from "./Loader";
import CategoryCard from "../components/CategoryCard";


const Menu = () => {
    let isLoading=useSelector((state)=>state.settings.loader)
    let dishes =useSelector((state)=> state.dish.sortedMenu)
    const categories=useSelector((state)=> state.dish.categories)

    const dispatch=useDispatch()

    useEffect(()=>{

    },[dishes,categories])


    const addToBasket=(id)=>{
        dispatch(addToBasketList({dishId:id}))
    }
    const addToFavList=(dish)=>{
        dispatch(addToFavouriteList({dish:dish}))
    }
    const sortByCategory=(category)=>{
        dispatch(setActiveCategory({category:category}))
    }

    return (
                    <SafeAreaView style={styles.container}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <View style={styles.categoryContainer}>
                                {
                                    categories && categories !== undefined &&
                                    categories.map((item,index)=>(
                                        <CategoryCard
                                            key={index}
                                            name={item.name}
                                            id={item.id}
                                            sortByCategory={sortByCategory}
                                        />
                                        )
                                    )
                                }
                            </View>
                        </ScrollView>
                        <ScrollView >
                            <View style={styles.dishList}>
                                {
                                    dishes&&dishes.length!==0&&
                                    dishes.map((item,index)=>(
                                        <Dish
                                            key={index}
                                            id={item.id}
                                            dish={item}
                                            price={item.cost}
                                            name={item.name}
                                            img={item.img}
                                            addToFavList={addToFavList}
                                            addToBasket={addToBasket}
                                        />
                                    ))
                                }
                            </View>
                        </ScrollView>
                        <NavBarBottom style={styles.navbar}/>
                    </SafeAreaView>


    );
};

const styles=StyleSheet.create({
    container:{
        flex:1,
        paddingTop: StatusBar.currentHeight,
        marginTop:4,
        // flexDirection:'column',
        // justifyContent:'center',
    },
    navbar:{
        alignSelf:'flex-end'
    },
    dishList:{
        // flex:1,
        flexDirection:'row',
        flexWrap:'wrap',
        alignItems:'stretch',
        justifyContent:'center',
    },
    categoryContainer:{
        flexDirection:'row',
        marginBottom:10,
        marginHorizontal:4,
    },
})
export default Menu;