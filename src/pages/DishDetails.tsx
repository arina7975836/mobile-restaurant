import React, {useEffect, useState} from 'react';
import {Image, Pressable, StyleSheet, View} from 'react-native'
import {Text} from 'react-native'
import {useRoute} from "@react-navigation/native";
import { AntDesign } from '@expo/vector-icons';

import  dishImg from '../../assets/dish.jpg';
import {colors} from "../../public/colors";
import NavBarBottom from "../components/NavBar";
import {addToBasketList} from "../redux/dishSlice";
import {useDispatch} from "react-redux";

const DishDetails = () => {
    const {dishName,price,img,id}=useRoute().params

    const dispatch=useDispatch()

    let [dishAmount,setDishAmount]=useState(1)

    useEffect(()=>{
    },[dishAmount])
    const increaseAmount=()=>{
        setDishAmount(++dishAmount);
    }

    const  decreaseAmount=()=>{
        setDishAmount(--dishAmount);

    }
    const addToBasket=(id)=>{
        dispatch(addToBasketList({dishId:id,amount:dishAmount}))
    }

    return (
        <View style={styles.detailContainer}>
           <View style={styles.header}></View>
            <View style={styles.imgContainer}>
                <Image style={styles.img} source={{uri:img}}/>
            </View>
            <View style={styles.mainContent}>
               <View style={styles.inputControl}>
                   <AntDesign style={{padding:5}}
                              name="minuscircleo"
                              size={30}
                              color={dishAmount===1 ? '#E8DEDE' : colors.darkGrey}
                              onPress={()=>decreaseAmount()}
                   />
                      <Text style={styles.amountText}>{dishAmount}</Text>
                   <AntDesign style={{padding:5}}
                              name="pluscircleo"
                              size={30}
                              color={colors.darkGrey}
                              onPress={()=>increaseAmount()}
                   />
               </View>
                <View style={styles.price}>
                    <Text style={{fontSize:20,fontWeight:'bold',marginRight:8}}>Цена:</Text>
                    <Text style={{fontSize:20,fontWeight:'normal'}}>{price}₽</Text>
                </View>
                <Text style={styles.dishName}>{dishName}</Text>
                <Pressable style={styles.buttonAdd} onPress={()=>addToBasket(id)}>
                    <Text style={styles.buttonAddText}>Добавить в корзину</Text>
                </Pressable>
            </View>
            <NavBarBottom/>
        </View>
    );
};

const styles=StyleSheet.create({
    detailContainer:{
        padding:0,
        marginTop:0,
        flex:1,
        flexDirection:'column',
        backgroundColor:colors.lightBrown,
    },
    header:{
        backgroundColor:colors.lightBrown,
        height:200,
        marginTop:0,
    },
    mainContent:{
        flex:1,
     flexDirection:'column',
     borderTopLeftRadius:25,
        borderTopRightRadius:25,
        backgroundColor:'#DADADA',
    },
    imgContainer:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    img:{
       width:300,
        height:250,
        borderRadius:50,
        position:'absolute',
        zIndex:100,
    },
    inputControl:{
        alignSelf:'center',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        padding:6,
        borderRadius:25,
        borderRightColor:colors.darkGrey,
        backgroundColor:'#EDE6E6',
        width:150,
        marginTop:200,
    },
    amountText:{
        fontSize:20,
        fontWeight:'normal',
        color:colors.darkGrey,
    },
    price:{
        marginTop:20,
        flexDirection:'row',
        justifyContent:'center',
        color:colors.darkGrey,
    },
    dishName:{
        fontSize:25,
        fontWeight:'bold',
        justifyContent:'flex-start',
        marginTop:50,
        paddingLeft:20,
    },
    buttonAdd:{
        alignSelf:'center',
        justifyContent:"center",
        alignItems:'center',
        padding:7,
        marginTop:50,
        backgroundColor:colors.lightBrown,
        width:250,
        borderRadius:25,
    },
    buttonAddText:{
        fontSize:20,
        fontWeight:'normal',
        color:colors.white,
    }

})
export default DishDetails;