import React, {useState} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import NavBarBottom from "../components/NavBar";
import {useDispatch, useSelector} from "react-redux";
import Dish from "../components/Dish";
import {StatusBar} from "expo-status-bar";
import FavDishCard from "../components/FavDishCard";
import {addToBasketList, deleteFromBasketList, deleteFromFavList} from "../redux/dishSlice";

const FavouriteItems = () => {
    const favDishes=useSelector((state)=>state.dish.favouriteList);

    const  dispatch=useDispatch()
    const addToBasket=(id)=>{
        dispatch(addToBasketList({dishId:id}))
    }
    const deleteDish=(id)=>{
        dispatch(deleteFromFavList({dishId:id}))
    }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView >
                <View style={styles.dishList}>
                    {
                        favDishes.length>0&&
                        favDishes.map((item,index)=>(
                            <FavDishCard
                                key={item.id}
                                dish={item}
                                id={item.id}
                                name={item.name}
                                price={item.cost}
                                img={item.img}
                                addToBasket={addToBasket}
                                deleteDish={deleteDish}
                            />
                        ))
                    }
                </View>
            </ScrollView>
            <NavBarBottom style={styles.navbar}/>
        </SafeAreaView>
    );
};

const styles=StyleSheet.create({
    container:{
        flex:1,
        paddingTop: StatusBar.currentHeight,
        marginTop:4,
    },
    navbar:{
        alignSelf:'flex-end'
    },
    dishList:{
        flexDirection:'column',
        width:StatusBar.currentWidth,
        flexWrap:'nowrap',
        alignItems:'center',
        justifyContent:'center',
    }
})
export default FavouriteItems;