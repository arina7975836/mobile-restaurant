import React, {useState} from 'react';
import { View,Text} from "react-native";
import {useNavigation} from "@react-navigation/native";
import {AntDesign, Feather} from '@expo/vector-icons';
import {StyleSheet} from "react-native";
import {useSelector} from "react-redux";
import {colors} from "../../public/colors";

const NavBarBottom = () => {
    const navigation = useNavigation();

    const [onFocusItem,setonFocusItem]=useState('1')
    const dishNumberInBasket=useSelector((state)=>state.dish.basketList.length)

    return (
        <View style={styles.container}>
            <AntDesign id='1' name="home" size={24} color={`${onFocusItem==='1' ? '#5F2518' : '#9A939B'}`}
                       onPress={()=>{
                           navigation.navigate('Menu')
                           setonFocusItem('1')
                       }} />
            <Feather name="bookmark" size={24} color={`${onFocusItem==='2' ? '#5F2518' : '#9A939B'}`}
                onPress={()=>{
                navigation.navigate('FavItems')
                setonFocusItem('2')
            } }
            />
            <View style={{flexDirection:'row'}}>
                <AntDesign id='3' name="shoppingcart" size={24} color={`${onFocusItem==='3' ? '#5F2518' : '#9A939B'}`}
                           onPress={()=>{
                               navigation.navigate('Basket')
                               setonFocusItem('3')
                           } }
                />
                <Text style={dishNumberInBasket>0 ?styles.dishNumber: {display:'none'}}>{dishNumberInBasket}</Text>
            </View>
            <AntDesign id='4' name="user" size={24} color={`${onFocusItem==='4' ? '#5F2518' : '#9A939B'}`}
                       onPress={()=>{
                           navigation.navigate('Profile')
                           setonFocusItem('4')
                       }}
            />
        </View>
    );
};

const styles=StyleSheet.create({
    container:{
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:'#fff',
        paddingVertical:20,
        paddingHorizontal:45,
        borderTopLeftRadius:25,
        borderTopRightRadius:25,
        alignSelf:'flex-end',
    },
    dishNumber:{
        alignSelf:'flex-start',
        zIndex:10,
        marginBottom:-2,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        padding:3,
        paddingHorizontal:7,
        borderRadius:25,
        fontSize:12,
        backgroundColor:colors.primary,
        color:colors.white,
        marginLeft:-3,
        marginTop:-9
    }
})

export default NavBarBottom;