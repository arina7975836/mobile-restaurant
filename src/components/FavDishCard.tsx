import React from 'react';
import {StyleSheet, Text, View, Image, Pressable} from 'react-native';
import {colors} from "../../public/colors";
import  dishImg from '../../assets/dish.jpg';
import {AntDesign} from "@expo/vector-icons";

const FavDishCard = ({id,name,price,img,addToBasket,deleteDish}) => {
    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Image style={styles.img} source={{uri:img}}/>
                <View style={styles.textContainer}>
                    <Text style={styles.dishName}>{name}</Text>
                    <Text style={styles.dishPrice}>{price}₽</Text>
                </View>
                <View style={styles.closeIcon}>
                    <AntDesign  name="closecircle" size={24} color={colors.red} onPress={()=>deleteDish(id)} />
                </View>
            </View>
            <Pressable style={styles.buttonAdd} onPress={()=>addToBasket(id)}>
                <Text style={styles.buttonText}>В корзину</Text>
            </Pressable>
        </View>
    );
};

const styles=StyleSheet.create({
    container:{
        flexDirection:'column',
        backgroundColor: colors.lightGrey,
        flex:1,
        width: '95%',
        marginHorizontal:5,
        marginTop:10,
        borderRadius:12,
        paddingHorizontal:5,
        paddingVertical:5,
    },
    content:{
        flexDirection:'row',

    },
    textContainer:{
        flex:2,
        flexDirection:'column',
        justifyContent:'flex-start',
        alignSelf:'center',
        marginLeft:20,
    },
    buttonAdd:{
        flex:1,
        flexDirection:'row',
        backgroundColor:colors.primary,
        color:colors.ligthGrey,
        height:40,
        borderRadius: 12,
        justifyContent:"center",
        alignItems:'center',
        marginTop:10,
    },
    buttonText:{
        // flex:1,
        flexDirection:'row',
        // backgroundColor:colors.primary,
        color:colors.lightGrey,
        fontSize:17,
        justifyContent:"center",
        alignSelf:'center',
    },
    img:{
     flex:1,
        height:110,
        width:100,
        borderRadius:25,
    },
    dishName:{
        fontSize:18,
        fontWeight:"bold",
        color: colors.darkGrey,
    },
    dishPrice:{
        fontSize:15,
        fontWeight:"normal",
    },
    closeIcon:{
        alignSelf:'flex-start',
    }
})
export default FavDishCard;