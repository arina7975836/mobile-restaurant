import React, {useEffect} from 'react';
import {Pressable, StyleSheet, Text, View} from "react-native";
import {colors} from "../../public/colors";
import {useSelector} from "react-redux";

const CategoryCard = ({name,id,sortByCategory}) => {
    const activeCategory=useSelector((state)=>state.dish.activeCategory)

    return (
        <Pressable style={activeCategory?.id===id ? styles.activeCtgr : styles.categoryItem}
                   onPress={()=>sortByCategory({id:id,name:name})}
        >
            <Text style={styles.textName}>{name}</Text>
        </Pressable>
    );
};

const styles=StyleSheet.create({
    categoryItem:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
      marginVertical:6,
        marginRight:6,
        backgroundColor:colors.lightBrown,
        borderRadius:25,
        padding:12,
        height:50
    },
    textName:{
        fontSize:20,
        fontWeight:'normal',
        color:colors.white,
    },
    activeCtgr:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        marginVertical:6,
        marginRight:6,
        borderRadius:25,
        padding:12,
        backgroundColor:colors.primary,
        height:55,
    }
})

export default CategoryCard;