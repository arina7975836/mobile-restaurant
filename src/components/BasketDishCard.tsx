import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, View,Text} from "react-native";
import { AntDesign } from '@expo/vector-icons';

import  dishImg from '../../assets/dish.jpg';
import {colors} from "../../public/colors";
import {useDispatch, useSelector} from "react-redux";
import {deleteFromBasketList, editDishAmount} from "../redux/dishSlice";
const BasketDishCard = ({key,id,name,price,img,fullPrice,amount}) => {
    let [dishAmount,setDishAmount]=useState(amount)
    const basketList=useSelector((state)=>state.dish.basketList)

    const dispatch=useDispatch()

    useEffect(()=>{
    },[dishAmount,basketList])
    const increaseAmount=()=>{
        setDishAmount(++dishAmount);
        dispatch(editDishAmount({dishId:id,amount:dishAmount}))
    }

    const  decreaseAmount=()=>{
        setDishAmount(--dishAmount);
        dispatch(editDishAmount({dishId:id,amount:dishAmount}))
    }

    const deleteDish=()=>{
        dispatch(deleteFromBasketList({dishId:id}))
    }

    return (
        <View style={styles.basketCardContainer}>
            <Image style={styles.img} source={{uri:img}}/>
            <View style={styles.dishInfo}>
                <Text style={styles.dishName}>{name}</Text>
                <Text style={styles.dishPrice}>{price}₽</Text>
            </View>
            <View style={styles.rightPart}>
                <View style={styles.closeIcon}>
                    <AntDesign  name="closecircle" size={24} color={colors.red} onPress={()=>deleteDish()} />
                </View>
                <View style={styles.amountControl}>
                    <AntDesign name="minuscircle"
                               size={28}
                               color={dishAmount==1 ? colors.notAllowedGrey : colors.middleGrey}
                               onPress={()=>decreaseAmount()}
                    />
                    <Text style={styles.amountText}>{dishAmount}</Text>
                    <AntDesign name="pluscircle"
                               size={28}
                               color={colors.middleGrey}
                               onPress={()=>increaseAmount()}
                    />
                </View>
                <Text style={{fontSize:18,color:colors.middleGrey}}>{dishAmount*price}₽</Text>
            </View>
        </View>
    );
};

const styles=StyleSheet.create({
    basketCardContainer:{
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        width:'94%',
        marginHorizontal:10,
        marginTop:8,
        padding:7,
        backgroundColor:colors.lightGrey,
        borderRadius:12,
    },
    img:{
        height:110,
        width:140,
        borderRadius:25,
    },
    dishInfo:{
        flexDirection:'column',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:15,
        flex:1,
    },
    dishName:{
        fontSize:20,
        fontWeight:'bold',
        overflow:'hidden',
    },
    dishPrice:{
        fontSize:16,
        fontWeight:'normal',
    },
    amountControl:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:10,
    },
    amountText:{
        fontSize:19,
        fontWeight:'normal',
        paddingHorizontal:5,
    },
    closeIcon:{
        justifyContent:'center',
        alignSelf:'flex-end'
    },
    rightPart:{
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'space-between'
    }
})
export default BasketDishCard;