import React from 'react';
import {Text, View, StyleSheet, Pressable, Image} from 'react-native';
import { Feather } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import {colors} from '../../public/colors.js';
import {useNavigation} from "@react-navigation/native";
import { Fontisto } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import {useSelector} from "react-redux";

const Dish = ({id,name,price,img,i,addToBasket,addToFavList,dish}) => {
 const navigation=useNavigation()

    const favDishesList=useSelector((state)=>state.dish.favouriteList)
    let isFavDish= favDishesList.find(item=>item.id===id)

    return (
        <View style={styles.container}>
            <Pressable  onPress={()=>navigation.navigate('Details',{dishName:name,price:price,img:img,id:id})}>
                {
                    !isFavDish ?
                <Feather name="bookmark" size={24} color="black"
                         style={styles.favIcon}
                         onPress={()=>addToFavList(dish)}
                />:
                    <FontAwesome name="bookmark" size={24} color="black"
                                 style={styles.favIcon}
                                 onPress={()=>addToFavList(dish)}
                    />
                }

                <View style={{alignSelf:'center',marginBottom:4}}>
                    <Image style={styles.img} source={{uri:img}}/>
                </View>
                {/*<Text style={{alignSelf:'center',marginBottom:4}}>{img}</Text>*/}
                <Text style={[styles.name,styles.text]}>{name}</Text>
                <View style={styles.bottomPart}>
                    <Text style={styles.price}>{price}₽</Text>
                </View>
            </Pressable>
            <View style={[styles.plusIcon]} >
                <Entypo name="plus" size={24} color={colors.white}  onPress={()=>addToBasket(id)}/>
            </View>
        </View>
    );
};

const styles=StyleSheet.create({
    container:{
        backgroundColor:colors.lightGrey,
        flexDirection:'column',
        paddingVertical:7,
        padding:0,
        marginBottom:7,
        marginLeft:7,
        width:'47%',
        borderRadius:25,
    },
    name:{
        alignSelf:'flex-start',
        overflow:'hidden',
        marginLeft:10,
        // textOverflow:'ellipsis',
        // whiteSpace:'nowrap',
    },
    price:{
     alignSelf:'flex-end',
        color:colors.middleGrey,
        fontSize:16,
        fontWeight:'normal'

    },
    plusIcon:{
        position:"absolute",
        zIndex:10,
        right:0,
        bottom:0,
        paddingVertical:7,
        paddingHorizontal:10,
        borderLeftColor:colors.lightBrown,
        borderTopColor:colors.lightBrown,
        borderTopLeftRadius:25,
        backgroundColor:colors.lightBrown,
        borderBottomRightRadius:25,

    },
    text:{
        fontWeight:'bold',
        fontSize:19,
        // fontFamily:'montserrat',
        color:colors.darkGrey,
    },
    bottomPart:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'flex-end',
        marginLeft:10,
    },
    img:{
        height:120,
        width:140,
        borderRadius:15,
        alignItems:'center',
        justifyContent:'center',
    },
    favIcon:{
        alignSelf:'flex-end',
        paddingRight:6,
        marginTop:2,
        marginRight:6,
        borderColor:'black'
        },
})

export default Dish;