import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useNavigation} from "@react-navigation/native";
import { AntDesign } from '@expo/vector-icons';


const HeaderTitle = ({darkBackGround=false}) => {
    const navigation = useNavigation();

    return (
        <View>
            <AntDesign  name="arrowleft" style={styles.arrowLeft} size={24} color={`${darkBackGround ? 'white' : 'black'} `} onPress={()=>navigation.goBack()} />
        </View>
    );
};

const styles=StyleSheet.create({
    arrowLeft:{
        paddingLeft:8,
        paddingRight:5,
    }
})
export default HeaderTitle;